'use strict';

/**
 * @ngdoc function
 * @name ngdemoApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ngdemoApp
 */
angular.module('ngdemoApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
